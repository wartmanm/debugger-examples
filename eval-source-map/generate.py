#!/usr/bin/python3
import os
import shutil
root = "src"
importdir = "imports"
originalfile = os.path.join(root, "original.js")
files = []

def clear():
    fullimportdir = os.path.join(root, importdir)
    if not os.path.exists(fullimportdir):
        os.mkdir(fullimportdir)
    for f in os.listdir(fullimportdir):
        if (os.path.isdir(fullimportdir + "/" + f)):
            shutil.rmtree(fullimportdir + "/" + f)
        else:
            os.unlink(fullimportdir + "/" + f)

def nested():
    clear()
    sizes = [10, 10, 30]
    lens = [len(str(x - 1)) for x in sizes]

    def get_filename(folder, number, depth):
        filename = str(number).zfill(lens[depth])
        return os.path.join(folder, filename)

    def go(folder, i):
        size = sizes[i]
        if i == len(sizes) - 1:
            # Webpack appears to resolve softlinks anywhere in import paths,
            # so we have to use hardlinked files and create real directories
            for number in range(sizes[i]):
                name = get_filename(folder, number, i) + ".js"
                os.link(originalfile, os.path.join(root, name))
                files.append(name)
        else:
            for number in range(sizes[i]):
                subfolder = get_filename(folder, number, i)
                os.mkdir(os.path.join(root, subfolder))
                go(subfolder, i + 1)
    go(importdir, 0)

nested()

output = open(os.path.join(root, "all-imports.js"), "w")
for i, f in enumerate(files):
    output.write(f"import * as import_{i} from \"./{f}\";\n")
output.write("\n")
output.write("const ignore = () => { };\n")
output.write("ignore([\n")
for i, f in enumerate(files):
    output.write(f"    import_{i},\n")
output.write("])\n")


