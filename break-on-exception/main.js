$.expr[":"].cool = function(elem) {
    return Array.from(elem.childNodes)
    .some(node => node.nodeType === Node.TEXT_NODE &&
        /[🆒😎]/.test(node.wholeText));
};

function breakOnExceptions() {
    $.Deferred().resolve().then(function () {
        $(":cool")[0].style.backgroundColor = "seashell";
        $(":uncool")[0].style.backgroundColor = "coral";
    });
}

function domBreakpoints() {
    const old = $("#hot");
    if (old.length) {
        old.remove();
    } else {
        const content = document.getElementById("content");
        $("#content").append("<p id='hot'>This paragraph is overheating 🥵</p>");
    }
}
